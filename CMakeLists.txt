cmake_minimum_required(VERSION 3.17)

set(PROJECT_NAME simple_shell)
project(${PROJECT_NAME} C)

set(CMAKE_C_STANDARD 11)

add_executable(${PROJECT_NAME}
        c-utilities/Logger.h     #c-c-utilities/StringBuilder.h      c-c-utilities/Instruments.h
        c-utilities/bin/Logger.o #c-c-utilities/bin/StringBuilder.o  c-c-utilities/bin/Instruments.o
        main.c)

#set(GCC_PROFILE_FUNCTIONS "-Wl,--no-as-needed -ldl -finstrument-functions -DINSTRUMENTATION_ON")
#set(CMAKE_C_FLAGS "${GCC_PROFILE_FUNCTIONS} ${CMAKE_C_FLAGS}")