#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include "c-utilities/Logger.h"

#define REMOVE_TRAILING_NEWLINE(string) \
    char *pos;\
    if ((pos = strchr(string, '\n')) != NULL)\
        *pos = '\0'

char **splitLine(char *currentLine);

int forkProcess(char **args);

//https://gist.github.com/leehambley/5589953
//https://docs.oracle.com/cd/E19455-01/806-4750/signals-7/index.html
//https://stackoverflow.com/questions/7171722/how-can-i-handle-sigchld
void signalHandler() {
    int waitStatus;
    pid_t processID;

    while (1) {
        processID = wait3(&waitStatus, WNOHANG, (struct rusage *) NULL);
        if (processID <= 0)
            return;
        else
            LOG_INFO("Child process was terminated.");
    }
}

int main() {

    signal(SIGCHLD, signalHandler);
    LOG_LEVEL(DEBUG);

    //https://www.reddit.com/r/AskProgramming/comments/64c6kx/c_why_is_getline_not_waiting_for_input/
    size_t lineLength;
    char *currentLine;
    char **args;
    int status;

    do {
        printf("simple-shell> ");
        //https://dev-notes.eu/2019/07/Get-a-line-from-stdin-in-C/
        getline(&currentLine, &lineLength, stdin);
        //https://stackoverflow.com/questions/2693776/removing-trailing-newline-character-from-fgets-input
        REMOVE_TRAILING_NEWLINE(currentLine);
        LOG_DEBUG("Parsed line: %s", currentLine);
        args = splitLine(currentLine);
        status = forkProcess(args);
    } while (status);
    return 0;
}

//https://www.educative.io/edpresso/splitting-a-string-using-strtok-in-c
//https://github.com/brenns10/lsh/blob/407938170e8b40d231781576e05282a41634848c/src/main.c
char **splitLine(char *currentLine) {
    int index = 0;
    char **tokens = malloc(128 * sizeof(char *));
    char *token;
    token = strtok(currentLine, " \n");
    while (token != NULL) {
        tokens[index] = token;
        index++;
        token = strtok(NULL, " \n");
    }
    // Arguments list needs to be NULL terminated
    tokens[index] = NULL;
    return tokens;
}

int allowedInBackground(char **args);

//https://brennan.io/2015/01/16/write-a-shell-in-c/
int forkProcess(char **args) {
    //https://www.programiz.com/c-programming/library-function/string.h/strcmp
    if (!strcmp(args[0], "exit")) return 0;
    int backgroundProcess = allowedInBackground(args);

    pid_t processID;
    processID = fork();
    if (processID == 0) {
        // Child process
        if (execvp(args[0], args) == -1) {
            LOG_WARNING("Execution error. (%s)", args[0]);
        }
        // exec() functions return only if an error has occurred.
        LOG_DEBUG("Child process failed. (%s)", args[0]);
        exit(0);
    } else if (processID < 0) {
        // Error forking
        LOG_WARNING("Forking error.");
    } else {
        LOG_DEBUG("Back to parent process");
        if (backgroundProcess) {
            LOG_DEBUG("Child process allowed in background... (%s)", args[0]);
            return 1;
        }
        // Wait on a child
        LOG_DEBUG("Waiting... (%s)", args[0]);
        wait(NULL);
    }
    return 1;
}

int allowedInBackground(char **args) {
    int i = 0;
    while (args[i] != NULL) {
        i++;
    }
    int bg = !strcmp(args[i - 1], "&");
    if (bg) args[i - 1] = NULL;
    return bg;
}
